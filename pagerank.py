import os
import random
import re
import sys
import numpy as np #added numpy library

DAMPING = 0.85
SAMPLES = 10000


def main():
    if len(sys.argv) != 2:
        sys.exit("Usage: python pagerank.py corpus")
    corpus = crawl(sys.argv[1])
    ranks = sample_pagerank(corpus, DAMPING, SAMPLES)
    print(f"PageRank Results from Sampling (n = {SAMPLES})")
    for page in sorted(ranks):
        print(f"  {page}: {ranks[page]:.4f}")
    ranks = iterate_pagerank(corpus, DAMPING)
    print(f"PageRank Results from Iteration")
    for page in sorted(ranks):
        print(f"  {page}: {ranks[page]:.4f}")


def crawl(directory):
    """
    Parse a directory of HTML pages and check for links to other pages.
    Return a dictionary where each key is a page, and values are
    a list of all other pages in the corpus that are linked to by the page.
    """
    pages = dict()

    # Extract all links from HTML files
    for filename in os.listdir(directory):
        if not filename.endswith(".html"):
            continue
        with open(os.path.join(directory, filename)) as f:
            contents = f.read()
            links = re.findall(r"<a\s+(?:[^>]*?)href=\"([^\"]*)\"", contents)
            pages[filename] = set(links) - {filename}

    # Only include links to other pages in the corpus
    for filename in pages:
        pages[filename] = set(
            link for link in pages[filename]
            if link in pages
        )

    return pages


def transition_model(corpus, page, damping_factor):
    """
    Return a probability distribution over which page to visit next,
    given a current page.

    With probability `damping_factor`, choose a link at random
    linked to by `page`. With probability `1 - damping_factor`, choose
    a link at random chosen from all pages in the corpus.
    """
    tmodel=dict() #defining transition model
    ncorpus=len(corpus) #counting pages in the corpus
    plinks=corpus[page] #set of links in the current page
    nlinks=len(plinks) #counting links in the current page
    
    if nlinks>0:

        prob_links=damping_factor/nlinks #prob of choosing any link of the page at random
        prob_ran=(1-damping_factor)/ncorpus #prob of choosing any page of the corpus randomly
        
        for k in corpus:
            tmodel[k]=prob_ran
            if k in plinks:
                tmodel[k]+=prob_links
    else:

        for k in corpus:
            tmodel[k]=1/ncorpus
    
    return tmodel


def sample_pagerank(corpus, damping_factor, n):
    """
    Return PageRank values for each page by sampling `n` pages
    according to transition model, starting with a page at random.

    Return a dictionary where keys are page names, and values are
    their estimated PageRank value (a value between 0 and 1). All
    PageRank values should sum to 1.
    """
    prs=dict() #pageranke by samples
    pages=list(corpus.keys()) #list of pages
    lsample=list() #sample list
    lsample.append(random.choices(pages).pop()) #first random sample
    s=1 #counter of samples
    while s<n:
       
       #Defining transition model of the last sample
        tmod=transition_model(corpus,lsample[-1],damping_factor)
        pages=list(tmod.keys())
        probt=list(tmod.values())
        #Generation a new sample
        newsamp=random.choices(population=pages,weights=probt,k=1)
        lsample.append(newsamp.pop()) #adding sample to the list of samples
        s+=1
    
    for k in corpus:
        prs[k]=lsample.count(k)/n #calculating pagerank (#occurrancies/#samples)
    
    return prs


def iterate_pagerank(corpus, damping_factor):
    """
    Return PageRank values for each page by iteratively updating
    PageRank values until convergence.

    Return a dictionary where keys are page names, and values are
    their estimated PageRank value (a value between 0 and 1). All
    PageRank values should sum to 1.
    """
    eps=0.001 #accuracy
    d=damping_factor #damping factor
    N=len(corpus) #size of corpus
    lm=linked_matrix(corpus) #matrix of adjency
    damp_random=(1-d)/N # probability of picking a random page of the corpus
    prob_random=np.full((N,1),damp_random) #vector of random probablity
    pri=np.full((N,1),1/N) #vector of Pageranks
    diff=True #boolean value that verify if we found pageranks with a wanted accuracy
    
    while diff:
        pr_old=pri.copy() #current pagerank values
        pri=prob_random+d*(np.dot(lm,pr_old)) #new pagerank values
        subs=np.absolute(pr_old-pri) #differency between old and new pagerank
        diff=(subs>eps).any() #verifying if the wanted accuracy was found

    #defining  pagerank dictionary with the found values
    ipagerank=dict(zip(list(corpus.keys()),pri.ravel().tolist()))
    
    return ipagerank


def linked_matrix(corpus):
    """
    Added function. This functions returs a matrix M that discribe 
    how the pages of the corpus are linked to each other (adjacency maxtrix).
    M is nxn and each of its components m(i,j)=(Numb.of links from page j to page i)/(Numb. links in page j)
    """
    ncor=len(corpus) #size of corpus
    keys=list(corpus.keys()) #pages
    links=list(corpus.values()) #links in each page
    lmatrix=np.zeros((ncor,ncor)) #matrix of adjacency
    
    for i in range(ncor): #loop through pages (colums of the lmatrix)
        if  len(links[i])!=0: #check if the current page has links to other pages of the corpus
            for k in links[i]: #loop through links (rows of matrix)
                j=keys.index(k)
                lmatrix[j,i]=1/len(links[i])
        else:
            for j in range(ncor):#loop through links (rows of matrix)
                lmatrix[j,i]=1/len(keys) #if there are not links on the current page, we suppouse there are links to all the pages of the corpus

    return lmatrix



if __name__ == "__main__":
    main()

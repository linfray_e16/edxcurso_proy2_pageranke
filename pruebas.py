import numpy as np
import pagerank as pr

dpf=0.85
###################Pruebas con corpus 0
#c0=pr.crawl("corpus0")
#c0["3.html"]={}
#print("corpus 0:",c0)

#pr.iterate_pagerank(c0,dpf)

# print("metodo A")
# print(m1)
# print("suma por columna", np.sum(m1,axis=0))
# print("metodo B")
# print(m2)
# print("suma por columna", np.sum(m2,axis=0))
# cpage="2.html"
# print("current page: ",cpage)
# tm0=pr.transition_model(c0,cpage,0.85)
# print("Transition model",tm0)
# probd=list(tm0.values())
# suma=sum(probd)
# print(probd,suma)

#rps=pr.sample_pagerank(c0,dpf,10000)

###################Pruebas con corpus 1
#c1=pr.crawl("corpus1")
#print("\corpus 1:",c1)
#pr.iterate_pagerank(c1,dpf)
# print("paginas")
# print(c1.keys())
# print("links")
# print(c1.values())
# m1=pr.linked_matrix_A(c1)
# m2=pr.linked_matrix_B(c1)
# print("metodo A")
# print(m1)
# print("metodo B")
# print(m2)
# print("son iguales",np.array_equal(m1,m2))
# cpage="bfs.html"
# print("current page: ",cpage)
# tm1=pr.transition_model(c1,cpage,0.85)
# print("Transition model",tm1)
# probd=list(tm1.values())
# suma=sum(probd)
# print(probd,suma)

#rps=pr.sample_pagerank(c1,dpf,1000)

###################Pruebas con corpus 1
c2=pr.crawl("corpus2")
print("\corpus 2:",c2)
pr.iterate_pagerank(c2,dpf)

# print("paginas")
# print(c2.keys())
# print("links")
# print(c2.values())
# m1=pr.linked_matrix_A(c2)
# m2=pr.linked_matrix_B(c2)
# print("metodo A")
# print(m1)
# print("metodo B")
# print(m2)
# print("son iguales",np.array_equal(m1,m2))
# cpage="recursion.html"
# print("current page: ",cpage)
# tm2=pr.transition_model(c2,cpage,0.85)
# print("Transition model",tm2)
# probd=list(tm2.values())
# suma=sum(probd)
# print(probd,suma)
rps=pr.sample_pagerank(c2,dpf,10000)
